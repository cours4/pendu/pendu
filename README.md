# Hangman

## What is it ?

Hangman is a game consisting in finding a word by guessing the letters that compose it.  

This project was realized in the framework of the module Info0306 of the university of Reims.

## Author

- Corentin - Student

## Prerequisites


- Android 8.0 or higher.
- **IOS not supported.**

## Apk

- Download the APk here:
